<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_search()
    {
        $product = Product::query()->with('availabilities')->first();
        $filter = [
            'sort' => ['column' => 'name', 'direction' => 'asc'],
            'search' => [
                ['column' => 'name', 'value' => str_split($product->name, 10)[0]],
                ['column' => 'description', 'value' => str_split($product->description, 10)[0]]
            ],
            'filter' => ['startDate' => now(), 'endDate' => now()->addWeeks(3)]
        ];
        $response = $this->json('GET', route('search'), $filter);

        $response->assertJson([[
            "title" => $product->name,
            "minimumPrice" => $product->price,
            "thumbnail" => $product->thumbnail,
        ]]);
        $response->assertStatus(200);
    }
}
