<?php

namespace App\Console\Commands;

use App\Services\TourService;
use Illuminate\Console\Command;

class StoreProductFromServer extends Command
{
    public function __construct(protected TourService $service)
    {
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command store product from server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->service->getDataFromServerAndStoreThem();
        return Command::SUCCESS;
    }
}
