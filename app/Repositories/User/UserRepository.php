<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepositoryClass;

class UserRepository extends BaseRepositoryClass implements UserRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getModel(): string
    {
        return User::class;
    }
}