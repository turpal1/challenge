<?php

namespace App\Repositories\Availability;

use App\Models\Availability;
use App\Repositories\BaseRepositoryClass;

class AvailabilityRepository extends BaseRepositoryClass implements AvailabilityRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function getModel(): string
    {
        return Availability::class;
    }
}