<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BaseRepositoryClass;

class ProductRepository extends BaseRepositoryClass implements ProductRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function getModel(): string
    {
        return Product::class;
    }
}