<?php

namespace App\Providers;


use App\Repositories\Availability\AvailabilityRepository;
use App\Repositories\Availability\AvailabilityRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\Contracts\AvailabilityServiceInterface;
use App\Services\Contracts\ProductServiceInterface;
use App\Services\Contracts\SearchServiceInterface;
use App\Services\Contracts\TourServiceInterface;
use App\Services\Contracts\UserServiceInterface;
use App\Services\AvailabilityService;
use App\Services\ProductService;
use App\Services\TourService;
use App\Services\SearchService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class InitServiceProvider extends ServiceProvider
{
    /**
     * @var array|string[]
     */
    public array $singletons = [
        //Repositories
        UserRepositoryInterface::class => UserRepository::class,
        ProductRepositoryInterface::class => ProductRepository::class,
        AvailabilityRepositoryInterface::class => AvailabilityRepository::class,

        //Services
        UserServiceInterface::class => UserService::class,
        ProductServiceInterface::class => ProductService::class,
        AvailabilityServiceInterface::class => AvailabilityService::class,
        SearchServiceInterface::class => SearchService::class,

        // tools
        TourServiceInterface::class => TourService::class,

        // others
    ];


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
