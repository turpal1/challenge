<?php

namespace App\Http\Requests\Search;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'filter' => ['array', 'nullable'],
            'filter.startDate' => ['date'],
            'filter.endDate' => ['date'],
            'sort' => ['array', 'nullable'],
            'sort.column' => Rule::in(['price', 'name', 'startDate', 'endDate']),
            'sort.direction' => Rule::in(['asc', 'desc']),
            'search' => ['array', 'required', 'min:1', 'max:2'],
            'search.*.column' => ['required', Rule::in(['name', 'description'])],
            'search.*.value' => ['string', 'required', 'max:20'],
        ];
    }
}
