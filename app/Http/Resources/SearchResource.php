<?php

namespace App\Http\Resources;

use App\Models\Availability;
use App\Models\Product;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        /* @var Product |  Availability | SearchResource $this */
        return [
            "title" => $this->name,
            "minimumPrice" => $this->price,
            "thumbnail" => $this->thumbnail,
        ];
    }
}
