<?php

namespace App\Services\Contracts;


use App\DTO\ProductDTO;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface ProductServiceInterface
{
    /**
     * @param ProductDTO $productDTO
     * @return Builder|Model|mixed
     */
    public function store(ProductDTO $productDTO);
}