<?php

namespace App\Services\Contracts;

interface SearchServiceInterface
{
    public function search(array $data);
}