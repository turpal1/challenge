<?php

namespace App\Services;

use App\DTO\ProductDTO;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Services\Contracts\ProductServiceInterface;

class ProductService implements ProductServiceInterface
{
    public function __construct(protected ProductRepositoryInterface $productRepository)
    {
    }

    public function store(ProductDTO $productDTO)
    {
        return $this->productRepository->baseQuery()->updateOrCreate(
            [
                'productable_id' => $productDTO->getProductableId()
            ],
            $productDTO->getProduct()
        );
    }
}