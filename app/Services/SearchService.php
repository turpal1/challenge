<?php

namespace App\Services;

use App\Repositories\Availability\AvailabilityRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Services\Contracts\SearchServiceInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SearchService implements SearchServiceInterface
{
    /**
     * @param ProductRepositoryInterface $productRepository
     * @param AvailabilityRepositoryInterface $availabilityRepository
     */
    public function __construct(
        protected ProductRepositoryInterface      $productRepository,
        protected AvailabilityRepositoryInterface $availabilityRepository
    )
    {
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function search(array $data)
    {
        $sort = $data['sort'];
        $startDate = $data['filter']['startDate'] ?: now();
        $endDate = $data['filter']['endDate'] ?: now()->addWeeks(2);

        $subqueryInsteadOfGroupBy = $this->availabilityRepository->baseQuery()
            ->where([
                ['start_time', '>=', $startDate],
                ['end_time', '<=', $endDate]
            ])->orderBy('price')
            ->groupBy('product_id');

        $query = $this->productRepository->baseQuery()
            ->joinSub($subqueryInsteadOfGroupBy, 'availabilities', 'availabilities.product_id', '=', 'products.id')
            ->orderBy($sort['column'], $sort['direction']);


        return $this->doSearch($data['search'], $query)
            ->get();
    }

    /**
     * @param array $filter
     * @return \Closure
     */
    private function filterFunction(array $filter): \Closure
    {
        return function (HasMany $query) use ($filter) {
            if (isset($filter) && !is_null($filter))
                return $query->where([
                    ['start_time', '>=', $filter['startDate']],
                    ['end_time', '<=', $filter['endDate']]
                ])->orderBy('price');

            return $query->where([
                ['start_time', '>=', now()],
                ['end_time', '<=', now()->addWeeks(2)]
            ])->orderBy('price');
        };
    }

    /**
     * @param array $search
     * @param Builder $query
     * @return Builder
     */
    private function doSearch(array $search, Builder $query): Builder
    {
        $filters = [];
        foreach ($search as $item) {
            $filters[] = [$item['column'], 'LIKE', "%{$item['value']}%"];
        }

        if (count($search) > 1) return $query->orWhere($filters);

        return $query->where($filters);
    }
}