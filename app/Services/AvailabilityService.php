<?php

namespace App\Services;

use App\Repositories\Availability\AvailabilityRepositoryInterface;
use App\Services\Contracts\AvailabilityServiceInterface;

class AvailabilityService implements AvailabilityServiceInterface
{
    public function __construct(protected AvailabilityRepositoryInterface $availabilityRepository)
    {
    }

    /**
     * @inheritDoc
     */
    public function store(array $data)
    {
        return $this->availabilityRepository->store($data);
    }

    /**
     * @inheritDoc
     */
    public function destroy($id)
    {
        return $this->availabilityRepository->destroy($id);
    }

    /**
     * @inheritDoc
     */
    public function list($request = null)
    {
        return $this->availabilityRepository->list();
    }

    /**
     * @inheritDoc
     */
    public function show($id)
    {
        return $this->availabilityRepository->show($id);
    }

    /**
     * @inheritDoc
     */
    public function update($id, array $data)
    {
        return $this->availabilityRepository->update($id, $data);
    }
}