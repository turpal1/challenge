<?php

namespace App\Services;

use App\Repositories\User\UserRepositoryInterface;
use App\Services\Contracts\UserServiceInterface;

class UserService implements UserServiceInterface
{
    public function __construct(protected UserRepositoryInterface $userRepository)
    {
    }

    /**
     * @inheritDoc
     */
    public function store(array $data)
    {
        return $this->userRepository->store($data);
    }

    /**
     * @inheritDoc
     */
    public function destroy($id)
    {
        return $this->userRepository->destroy($id);
    }

    /**
     * @inheritDoc
     */
    public function list($request = null)
    {
        return $this->userRepository->list();
    }

    /**
     * @inheritDoc
     */
    public function show($id)
    {
        return $this->userRepository->show($id);
    }

    /**
     * @inheritDoc
     */
    public function update($id, array $data)
    {
        return $this->userRepository->update($id, $data);
    }
}