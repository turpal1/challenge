<?php

namespace App\Services;

use App\DTO\ProductDTO;
use App\Models\Product;
use App\Services\Contracts\ProductServiceInterface;
use App\Services\Contracts\TourServiceInterface;
use App\Tools\ToursAPIService;

class TourService implements TourServiceInterface
{
    protected array $tours = [];
    protected array $toursPrice = [];

    /**
     * @param ToursAPIService $toursAPIService
     * @param ProductServiceInterface $productService
     */
    public function __construct(
        protected ToursAPIService         $toursAPIService,
        protected ProductServiceInterface $productService,
    )
    {
        $this->tours = $this->toursAPIService->getTours();
        $this->toursPrice = $this->toursAPIService->getToursWithPrice(now()->format('Y-m-d'));
    }

    /**
     * @return void
     */
    public function getDataFromServerAndStoreThem()
    {
        foreach ($this->tours as $tour) {
            $product = $this->productService->store(new ProductDTO($this->toursAPIService->getTourDetail($tour['id'])));
        }
        $this->syncAvailablities($product);
    }

    /**
     * @param Product $product
     * @return void
     */
    public function syncAvailablities(Product $product)
    {
        $availabilityStart = $this->toursAPIService->getAvailability($product->productable_id, now()->format('Y-m-d'));
        $availabilityEnd = $this->toursAPIService->getAvailability($product->productable_id, now()->addWeeks(2)->format('Y-m-d'));
        if ($availabilityStart && $availabilityEnd) $product->availabilities()->updateOrCreate(
            [
                'price' => $this->getPriceOfTour($product->productable_id),
            ],
            [
                'start_time' => now(),
                'end_time' => now()->addWeeks(2),
                'price' => $this->getPriceOfTour($product->productable_id),
            ]);
    }

    /**
     * @param string $tourId
     * @return int
     */
    private function getPriceOfTour(string $tourId): int
    {
        foreach ($this->toursPrice as $price) {
            if ($price['tourId'] === $tourId) return $price['price'];
        }

        return 0;
    }
}