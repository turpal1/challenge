<?php

namespace App\DTO;

class ProductDTO
{
    public function __construct(protected array $responseData, protected string $type = 'tour')
    {
        throw_if(empty($this->responseData));
    }

    /**
     * @return array
     */
    public function getProduct(): array
    {
        return [
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'thumbnail' => $this->getThumbnail(),
            'productable_type' => $this->getType(),
            'productable_id' => $this->getProductableId(),
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->responseData['title'] ?? '';
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->responseData['description'] ?? '';
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->getThumbnailFromResponse();
    }

    /**
     * @return string
     */
    public function getProductableId(): string
    {
        return $this->responseData['id'];
    }

    /**
     * @return string
     */
    private function getThumbnailFromResponse(): string
    {
        $photos = $this->responseData['photos'];

        foreach ($photos as $photo) {
            if ($photo['type'] === 'thumbnail') return $photo['url'];
        }
        return $photos[0]['url'] ?? '';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}