<?php

namespace App\Enums;

class HttpStatusCode
{
    const STATUS_CODE_OK                    = 200;
    const STATUS_CODE_CREATED               = 201;
    const STATUS_CODE_NO_CONTENT            = 204;
    const STATUS_CODE_NOT_FOUND             = 404;
    const STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
    const STATUS_CODE_UNAUTHENTICATED       = 401;
    const STATUS_CODE_METHOD_NOT_ALLOWED    = 405;
    const STATUS_CODE_FORBIDDEN             = 403;
    const STATUS_CODE_BAD_REQUEST           = 400;
    const STATUS_CODE_UNPROCESSABLE_ENTITY  = 402;
}

