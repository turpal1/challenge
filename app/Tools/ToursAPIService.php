<?php

namespace App\Tools;


use Illuminate\Support\Facades\Http;

class ToursAPIService
{
    protected string $baseUrl = 'http://161.35.193.238:3006/api/';
    protected string $toursUrl;
    protected string $tourDetailUrl;
    protected string $toursWithPriceUrl;
    protected string $availabilityUrl;

    public function __construct()
    {
        $this->toursUrl = 'tours';
        $this->tourDetailUrl = 'tours/:id';
        $this->toursWithPriceUrl = 'tour-prices?travelDate=:travelData';
        $this->availabilityUrl = 'tours/:id/availability?travelDate=:travelData';
    }


    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $url
     * @param array $data
     * @return string
     */
    public function makeUrlWithparams(string $url, array $data = []): string
    {
        if (!empty($data)) return $this->baseUrl . preg_replace_array('/:\w+/i', $data, $url);

        return $this->baseUrl . $url;
    }

    /**
     * @return array|mixed
     */
    public function getTours(): array
    {
        return Http::get($this->makeUrlWithparams($this->toursUrl))->json();
    }

    /**
     * @param string $id
     * @return array
     */
    public function getTourDetail(string $id): array
    {
        return Http::get($this->makeUrlWithparams($this->tourDetailUrl, [$id]))->json();
    }

    /**
     * @param string $travelDate
     * @return array
     */
    public function getToursWithPrice(string $travelDate): array
    {
        return Http::get($this->makeUrlWithparams($this->toursWithPriceUrl, [$travelDate]))->json();
    }

    /**
     * @param int $id
     * @param string $travelDate
     * @return array
     */
    public function getAvailability(string $id, string $travelDate): array
    {
        return Http::get($this->makeUrlWithparams($this->availabilityUrl, [$id, $travelDate]))->json();
    }
}