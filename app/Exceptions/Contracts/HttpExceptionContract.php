<?php

namespace App\Exceptions\Contracts;

interface HttpExceptionContract
{
    public static function throwException(int $statusCode, string $message = '');
}
