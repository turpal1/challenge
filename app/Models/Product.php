<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $thumbnail
 * @property string $productable_id
 * @property string $productable_type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read  Availability $availabilities
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'thumbnail', 'productable_type', 'productable_id'];

    /**
     * @return HasMany
     */
    public function availabilities(): HasMany
    {
        return $this->hasMany(Availability::class);
    }
}
