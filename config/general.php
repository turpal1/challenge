<?php

/*
 * global config for user in project
 */
return [

    'paginate_number' => 20,

    'exceptions_messages' => [
        'fa' => [
            'success_request' => 'Your request has been successfully completed',
            'not_valid_data' => 'The information sent is not valid',
            'item_not_found' => 'We did not find any data for you!',
            'success_delete' => 'Removed successfully!',
            'an_error_occurred' => 'The request encountered a problem',
            'internal_error' => 'Sorry, the server has encountered a problem, please try again later.',
            'query_error' => 'Runtime error',
            'exchange_response_error' => 'Exchange response error',
            'error_cancel_order' => ' You can not cancel this order',
            'order_min_notional_error' => 'The amount requested is less than the limit',
            'error_order_action' => 'This order does not have the ability to perform this operation!',
            'symbol_not_found' => 'Information not found!',
            'golang_server_not_available' => 'Golang server is not available!',
            'class_not_found' => 'Class not found, please check your code!',
            'parent_and_child_order_side_error' => "You can't be on the same side as the parent and the child!",
        ]

    ],
];

